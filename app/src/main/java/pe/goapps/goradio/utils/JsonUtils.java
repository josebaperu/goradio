package pe.goapps.goradio.utils;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import pe.goapps.goradio.model.Radio;

import static pe.goapps.goradio.utils.Constants.JSON_FILE;

public class JsonUtils {

    public static List<Radio> getRadioListFromJsonFile(final Context ctx) {
        try {
            final InputStream is = ctx.getAssets().open(JSON_FILE);
            final int size = is.available();
            final byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            return parseJSONtoRadios(new String(buffer, "UTF-8"));
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private static List<Radio> parseJSONtoRadios(final String jsonStr) {
        final List<Radio> radios = new ArrayList<>();
        try {
            final JSONArray objects = new JSONArray(jsonStr);
            for (int i = 0; i < objects.length(); i++) {
                JSONObject json = objects.getJSONObject(i);
                Radio radio = new Radio();
                radio.setId(json.getInt(Radio.ID));
                radio.setStation(json.getString(Radio.STATION));
                radio.setCountry(json.getString(Radio.COUNTRY));
                radio.setImg(json.getString(Radio.IMG));
                radio.setUrl(json.getString(Radio.URL));
                radios.add(radio);
            }
            return radios;

        } catch (JSONException e) {
            e.printStackTrace();
            return radios;
        }
    }
}
