package pe.goapps.goradio.utils;

public class Constants {
    public final static String PLAYING = "PLAYING";
    public final static String PAUSED = "PAUSED";
    public final static int WAITFORPAUSE = 150000;
    public final static int WAITFORSHUTDOWN = 300000;
    public final static int INTERVAL = 1000;
    public final static String JSON_FILE = "data.json";

    public interface ACTION {
        String MAIN_ACTION = "playerservice.action.main";
        String TOGGLE_PLAYBACK = "playerservice.action.toggle";
        String PREV_ACTION = "playerservice.action.prev";
        String PLAY_ACTION = "playerservice.action.play";
        String NEXT_ACTION = "playerservice.action.next";
        String STARTFOREGROUND_ACTION = "playerservice.action.startforeground";
        String STOPFOREGROUND_ACTION = "playerservice.action.stopforeground";
    }

    public interface NOTIFICATION_ID {
        int FOREGROUND_SERVICE = 101;
        int FOREGROUND_SERVICE_OREO = 102;
    }
}
