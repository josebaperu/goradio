package pe.goapps.goradio.store;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.List;

import pe.goapps.goradio.RadioApp;
import pe.goapps.goradio.model.Radio;
import pe.goapps.goradio.utils.JsonUtils;

import static android.content.Context.MODE_PRIVATE;

public class AppStorage {
    private static AppStorage appStorage;

    private AppStorage() {
    }

    ;
    private final Context context = RadioApp.getContext();
    private final List<Radio> radioList = JsonUtils.getRadioListFromJsonFile(context);

    final SharedPreferences settings = context.getSharedPreferences("GORADIO", MODE_PRIVATE);
    SharedPreferences.Editor editor;

    public static AppStorage getInstance() {
        if (appStorage == null) {
            appStorage = new AppStorage();
        }
        return appStorage;
    }

    public boolean isPlaybackOn() {
        return settings.getBoolean("IS_PLAYBACK_ON", false);
    }

    public void setIsPlaybackOn(final boolean b) {
        editor = settings.edit();
        editor.putBoolean("IS_PLAYBACK_ON", b);
        editor.apply();
    }

    public List<Radio> getRadioList() {
        return radioList;
    }

    public boolean isFirstRun() {
        if (settings.getBoolean("firstRun", true)) {
            editor = settings.edit();
            editor.putBoolean("firstRun", false);
            editor.apply();
            return true;
        } else {
            return false;
        }
    }


    public void setCurrentStation(final int i) {
        editor = settings.edit();
        editor.putString(Radio.STATION, radioList.get(i).getStation());
        editor.putInt(Radio.ID, radioList.get(i).getId());
        editor.apply();
    }

    public void onExit() {
        setIsPlaybackOn(false);
    }

    public String getCurrentStation() {
        return settings.getString(Radio.STATION, radioList.get(0).getStation());
    }

    public int getCurrentId() {
        return settings.getInt(Radio.ID, radioList.get(0).getId());
    }

    public Radio getCurrentRadio() {
        return radioList.get(getCurrentId());
    }


}
