package pe.goapps.goradio.adapters;

import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import pe.goapps.goradio.R;
import pe.goapps.goradio.RadioApp;
import pe.goapps.goradio.model.Radio;
import pe.goapps.goradio.services.PlayerService;
import pe.goapps.goradio.store.AppStorage;
import pe.goapps.goradio.utils.Constants;

public class RadiosAdapter extends RecyclerView.Adapter<RadiosAdapter.MyViewHolder> {

    private final AppStorage appStorage = AppStorage.getInstance();
    private final Intent service;

    class MyViewHolder extends RecyclerView.ViewHolder {
        final TextView title, country;
        final ImageView thumbnail, overflow;


        MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            country = view.findViewById(R.id.count);
            thumbnail = view.findViewById(R.id.thumbnail);
            overflow = view.findViewById(R.id.overflow);
        }
    }

    public RadiosAdapter() {
        service = new Intent(RadioApp.getContext(), PlayerService.class);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.radio_card, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Radio radio = appStorage.getRadioList().get(position);
        holder.title.setText(radio.getStation());
        holder.country.setText(radio.getCountry());
        Glide.with(RadioApp.getContext()).load(RadioApp.getContext().getResources().getIdentifier("@drawable/" + radio.getImg(), null, RadioApp.getContext().getPackageName())).into(holder.thumbnail);
        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appStorage.setCurrentStation(position);
                service.setAction(Constants.ACTION.PLAY_ACTION);
                RadioApp.getContext().startService(service);
            }
        });
    }

    /**
     * Showing popup menu when tapping on 3 dots
     */
    private void showPopupMenu(View view) {
        // inflate menu
        final PopupMenu popup = new PopupMenu(RadioApp.getContext(), view);
        final MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_album, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
        popup.show();
    }

    /**
     * Click listener for popup menu items
     */
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        @Override
        public boolean onMenuItemClick(final MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_add_favourite:
                    Toast.makeText(RadioApp.getContext(), "Add to favourite", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.action_play_next:
                    Toast.makeText(RadioApp.getContext(), "Play next", Toast.LENGTH_SHORT).show();
                    return true;
                default:
            }
            return false;
        }
    }

    @Override
    public int getItemCount() {
        return appStorage.getRadioList().size();
    }
}
