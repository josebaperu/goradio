package pe.goapps.goradio.receivers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.Date;

import pe.goapps.goradio.RadioApp;
import pe.goapps.goradio.services.PlayerService;
import pe.goapps.goradio.store.AppStorage;
import pe.goapps.goradio.utils.Constants;


public class CallReceiver extends PhoneCallReceiver {
    private final Intent service;
    private final Context context;
    private static final String LOG_TAG = "CallReceiver";
    private final AppStorage appStorage;

    public CallReceiver() {
        context = RadioApp.getContext();
        service = new Intent(context, PlayerService.class);
        appStorage = AppStorage.getInstance();
        service.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
    }

    @Override
    protected void onIncomingCallStarted(final Context ctx, final String number, final Date start) {
        Log.i(LOG_TAG, "incoming started");
        servicePerformerOnRinging();
    }

    @Override
    protected void onOutgoingCallStarted(final Context ctx, final String number, final Date start) {
        Log.i(LOG_TAG, "outgoing started");
        servicePerformerOnRinging();
    }

    @Override
    protected void onIncomingCallEnded(final Context ctx, final String number, final Date start, final Date end) {
        Log.i(LOG_TAG, "incoming ended");
        servicePerformerOnEnd();
    }

    @Override
    protected void onOutgoingCallEnded(final Context ctx, final String number, final Date start, final Date end) {
        Log.i(LOG_TAG, "outgoing ended");
        servicePerformerOnEnd();
    }

    @Override
    protected void onMissedCall(final Context ctx, final String number, final Date start) {
        Log.i(LOG_TAG, "missed ended");
        servicePerformerOnEnd();
    }

    private void servicePerformerOnRinging() {
        if (appStorage.isPlaybackOn()) {
            service.setAction(Constants.ACTION.TOGGLE_PLAYBACK);
            context.startService(service);
        }
    }

    private void servicePerformerOnEnd() {
        if (!appStorage.isPlaybackOn()) {
            service.setAction(Constants.ACTION.TOGGLE_PLAYBACK);
            context.startService(service);
        }
    }
}
