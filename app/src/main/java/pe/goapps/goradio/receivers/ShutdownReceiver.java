package pe.goapps.goradio.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import pe.goapps.goradio.store.AppStorage;

public class ShutdownReceiver extends BroadcastReceiver {
    private final AppStorage appStorage = AppStorage.getInstance();
    private static final String LOG_TAG = "ShutdownReceiver";

    @Override
    public void onReceive(final Context context, final Intent intent) {

        if (intent.getAction().equals("android.intent.action.ACTION_SHUTDOWN") || intent.getAction().equals("android.intent.action.ACTION_SHUTDOWN"))
            appStorage.setIsPlaybackOn(false);
    }
}
