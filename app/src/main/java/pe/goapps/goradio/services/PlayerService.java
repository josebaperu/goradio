package pe.goapps.goradio.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.media.VolumeProviderCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import pe.goapps.goradio.store.AppStorage;
import pe.goapps.goradio.utils.Constants;
import pe.goapps.goradio.views.PlayerActivity;

import static pe.goapps.goradio.utils.Constants.INTERVAL;
import static pe.goapps.goradio.utils.Constants.PLAYING;
import static pe.goapps.goradio.utils.Constants.WAITFORSHUTDOWN;


public class PlayerService extends Service implements com.google.android.exoplayer2.Player.EventListener {
    private CountDownTimer cTimer = null;
    private int radioID;
    private AppStorage appStorage;
    private static final String LOG_TAG = "PlayerService";
    private Intent localIntent;
    private SimpleExoPlayer player;
    private ExtractorMediaSource mediaSource;
    private MediaSessionCompat mMediaSessionCompat;
    private BroadcastReceiver mPowerKeyReceiver;
    private AudioManager audioManager;
    private VolumeProviderCompat volumeProviderCompat;
    private String CHANNEL_ID;
    private long startPrevious;
    private long startNext;

    @Override
    public void onCreate() {
        super.onCreate();
        startNext = 0;
        startPrevious = 0;
        appStorage = AppStorage.getInstance();
        localIntent = new Intent();
        appStorage.setIsPlaybackOn(false);
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        radioID = appStorage.getCurrentId();
        mMediaSessionCompat = new MediaSessionCompat(this, LOG_TAG);
        mMediaSessionCompat.setFlags(MediaSessionCompat.FLAG_HANDLES_QUEUE_COMMANDS | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS | MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS);
        mMediaSessionCompat.setCallback(new MediaSessionCompat.Callback() {


            @Override
            public boolean onMediaButtonEvent(Intent mediaButtonEvent) {

                final KeyEvent event = mediaButtonEvent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
                final String intentAction = mediaButtonEvent.getAction();
                final boolean condition = event != null &&
                        Intent.ACTION_MEDIA_BUTTON.equals(intentAction);

                if (condition && event.getAction() == KeyEvent.ACTION_DOWN) {
                    togglePlayBack();
                }

                Log.i(LOG_TAG, intentAction);
                return super.onMediaButtonEvent(mediaButtonEvent);
            }
        });
        mMediaSessionCompat.setPlaybackState(new PlaybackStateCompat.Builder()
                .setState(PlaybackStateCompat.STATE_PLAYING, 0, 0) //you simulate a player which plays something.
                .build());

        mMediaSessionCompat.setActive(true);
        registerScreenReceiver();
        volumeProviderCompat = getVolumeProvider();
    }

    @Override
    public int onStartCommand(final Intent intent, final int flags, final int startId) {
        final String action = intent.getAction();
        networkMessageCheck();
        if (action != null) {
            if (action.equals(Constants.ACTION.STARTFOREGROUND_ACTION)) {
                Log.i(LOG_TAG, "Start Foreground Intent");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                    startForegroundOreo();
                else
                    startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, getNotification());
                setMediaPlayer();
                localIntent.setAction("updateplayer");
                sendBroadcast(localIntent);
                Log.i(LOG_TAG, "service started");
                cancelTimer();
                serviceCountDownTimer();
            } else if (action.equals(Constants.ACTION.PREV_ACTION) && isNetworkAvailable()) {
                cancelTimer();
                serviceCountDownTimer();
                playPrevious();
                Log.i(LOG_TAG, "Clicked Previous");
            } else if (action.equals(Constants.ACTION.NEXT_ACTION) && isNetworkAvailable()) {
                cancelTimer();
                serviceCountDownTimer();
                playNext();
                Log.i(LOG_TAG, "Clicked Next");
            } else if (action.equals(Constants.ACTION.PLAY_ACTION) && isNetworkAvailable()) {
                Log.i(LOG_TAG, "Clicked Play");
                cancelTimer();
                serviceCountDownTimer();
                playBack();
            } else if (action.equals(Constants.ACTION.TOGGLE_PLAYBACK) && isNetworkAvailable()) {
                Log.i(LOG_TAG, "Clicked toggle Playback");
                cancelTimer();
                serviceCountDownTimer();
                togglePlayBack();
            } else if (action.equals(Constants.ACTION.STOPFOREGROUND_ACTION)) {
                cancelTimer();
                destroyService();
            }
        }

        return START_STICKY;
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private void startForegroundOreo() {

        CHANNEL_ID = "pe.goapps.goradio.oreoid";
        final String channelName = "radiochannel";
        final NotificationChannel chan = new NotificationChannel(CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        final NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);
        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE_OREO, getNotification());

    }

    private void destroyService() {
        Log.i(LOG_TAG, "Stop Foreground Intent");
        localIntent.setAction("exit");
        sendBroadcast(localIntent);
        unregisterScreenReceiver();
        mediaSource = null;
        player.stop();
        player.release();
        player = null;
        stopForeground(true);
        stopSelf();

    }

    private void playNext() {
        int idx = appStorage.getCurrentId() + 1;
        if (idx > appStorage.getRadioList().size() - 1) {
            idx = 0;
        }
        afterFlickingStations(idx);
    }

    private void playPrevious() {
        int idx = appStorage.getCurrentId() - 1;
        if (idx < 0) {
            idx = appStorage.getRadioList().size() - 1;
        }
        afterFlickingStations(idx);
    }

    private void afterFlickingStations(int idx) {
        appStorage.setCurrentStation(idx);
        Log.i(LOG_TAG, "station : " + appStorage.getCurrentStation());
        playBack();
    }

    private void togglePlayBack() {
        mediaSource = null;
        player.release();
        setMediaPlayer();
        if (appStorage.isPlaybackOn()) {
            player.setPlayWhenReady(false);
        } else {
            player.setPlayWhenReady(true);
        }
        appStorage.setIsPlaybackOn(!appStorage.isPlaybackOn());
        appStorage.setCurrentStation(radioID);
        updateNotification();
        localIntent.setAction("updateplayer");
        sendBroadcast(localIntent);
    }


    private boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void serviceCountDownTimer() {

        cTimer = new CountDownTimer(WAITFORSHUTDOWN, INTERVAL) {

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                if (!appStorage.isPlaybackOn()) {
                    destroyService();
                    System.exit(0);
                } else if (!isNetworkAvailable() || !audioManager.isMusicActive()) {
                    pausePlayer();
                    cancelTimer();
                    serviceCountDownTimer();
                }
            }

        }.start();
    }

    void cancelTimer() {
        if (cTimer != null) {
            cTimer.cancel();
            cTimer = null;
        }
    }

    private void networkMessageCheck() {
        if (!isNetworkAvailable()) {
            if (appStorage.isPlaybackOn()) {
                togglePlayBack();
            }
            Toast.makeText(this, "Internet / Data Unavailable", Toast.LENGTH_LONG).show();
        }

    }


    private void pausePlayer() {
        mediaSource = null;
        player.release();
        setMediaPlayer();
        player.setPlayWhenReady(false);
        appStorage.setIsPlaybackOn(false);
        updateNotification();
        localIntent.setAction("updateplayer");
        sendBroadcast(localIntent);
    }


    private void playBack() {
        if (radioID != appStorage.getCurrentId()) {
            mediaSource = null;
            player.release();
            setMediaPlayer();
            radioID = appStorage.getCurrentId();
            player.setPlayWhenReady(true);
            appStorage.setIsPlaybackOn(true);
            appStorage.setCurrentStation(radioID);
            localIntent.setAction("updateplayer");
            sendBroadcast(localIntent);
        }
        updateNotification();
    }

    private void updateNotification() {

        final Notification notification = getNotification();
        notification.flags = Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL;

        final NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (mNotificationManager != null) {
            mNotificationManager.notify(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ? Constants.NOTIFICATION_ID.FOREGROUND_SERVICE_OREO : Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, notification);
        }
    }


    private Notification getNotification() {
        final Intent notificationIntent = new Intent(this, PlayerActivity.class);
        notificationIntent.setAction(Constants.ACTION.MAIN_ACTION);

        final Intent deletePendingIntent = new Intent(this, PlayerService.class);
        deletePendingIntent.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
        final PendingIntent pdeletePendingIntent = PendingIntent.getService(this, 0, deletePendingIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        final Intent previousIntent = new Intent(this, PlayerService.class);
        previousIntent.setAction(Constants.ACTION.PREV_ACTION);
        final PendingIntent ppreviousIntent = PendingIntent.getService(this, 0, previousIntent, 0);

        final Intent toggleIntent = new Intent(this, PlayerService.class);
        toggleIntent.setAction(Constants.ACTION.TOGGLE_PLAYBACK);
        final PendingIntent ptoggleIntent = PendingIntent.getService(this, 0, toggleIntent, 0);

        final Intent nextIntent = new Intent(this, PlayerService.class);
        nextIntent.setAction(Constants.ACTION.NEXT_ACTION);
        final PendingIntent pnextIntent = PendingIntent.getService(this, 0, nextIntent, 0);
        final int dynamicIcon = this.getResources().getIdentifier(appStorage.getCurrentRadio().getImg(), "drawable", this.getPackageName());
        final Bitmap icon = BitmapFactory.decodeResource(getResources(), dynamicIcon);

        return new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(null == appStorage.getCurrentRadio() ? "" : appStorage.getCurrentRadio().getStation())
                .setTicker("GoRadio")
                .setContentText(null == appStorage.getCurrentRadio() ? "" : !appStorage.isPlaybackOn() ? Constants.PAUSED : PLAYING)
                .setSmallIcon(android.R.color.transparent)
                .setColor(Color.parseColor("#F50057"))
                .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setOngoing(true)
                .setWhen(0)
                .setDeleteIntent(pdeletePendingIntent)
                .setPriority(Notification.PRIORITY_MAX)
                .addAction(android.R.drawable.ic_media_previous, "Previous", ppreviousIntent)
                .addAction(null == appStorage.getCurrentRadio() ? android.R.drawable.ic_media_play : !appStorage.isPlaybackOn() ? android.R.drawable.ic_media_play : android.R.drawable.ic_media_pause, null == appStorage.getCurrentRadio() ? "Play" : !appStorage.isPlaybackOn() ? "Play" : "Pause", ptoggleIntent)
                .addAction(android.R.drawable.ic_media_next, "Next", pnextIntent)
                .build();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMediaSessionCompat.setActive(false);
        mMediaSessionCompat.release();
        Log.i(LOG_TAG, "onDestroy");
    }


    private void setMediaPlayer() {
        final DefaultBandwidthMeter defaultBandwidthMeter = new DefaultBandwidthMeter();
        final DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, "@string/app_name"), defaultBandwidthMeter);

        mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(appStorage.getCurrentRadio().getUrl()));

        final DefaultTrackSelector trackSelector = new DefaultTrackSelector(defaultBandwidthMeter);
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
        player.prepare(mediaSource);
        stopForeground(false);
    }

    @Override
    public IBinder onBind(final Intent intent) {
        return null;
    }

    private void registerScreenReceiver() {
        final IntentFilter IntentFilter = new IntentFilter();
        IntentFilter.addAction(Intent.ACTION_SCREEN_ON);
        IntentFilter.addAction(Intent.ACTION_SCREEN_OFF);

        mPowerKeyReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                final String strAction = intent.getAction();
                if (strAction != null) {
                    if (strAction.equals(Intent.ACTION_SCREEN_OFF)) {
                        mMediaSessionCompat.setPlaybackToRemote(volumeProviderCompat);

                    }
                    if (strAction.equals(Intent.ACTION_SCREEN_ON)) {
                        mMediaSessionCompat.setPlaybackToLocal(AudioManager.STREAM_MUSIC);
                    }
                }
            }
        };

        getApplicationContext().registerReceiver(mPowerKeyReceiver, IntentFilter);
    }

    private void unregisterScreenReceiver() {
        final int apiLevel = Build.VERSION.SDK_INT;

        if (apiLevel >= 7) {
            try {
                getApplicationContext().unregisterReceiver(mPowerKeyReceiver);
            } catch (IllegalArgumentException e) {
                mPowerKeyReceiver = null;
            }
        } else {
            getApplicationContext().unregisterReceiver(mPowerKeyReceiver);
            mPowerKeyReceiver = null;
        }
    }

    private VolumeProviderCompat getVolumeProvider() {
        return new VolumeProviderCompat(VolumeProviderCompat.VOLUME_CONTROL_RELATIVE,

                /*max volume*/100, /*initial volume level*/
                audioManager.getStreamVolume(AudioManager.STREAM_MUSIC)) {

            @Override
            public void onAdjustVolume(final int direction) {
                Log.i(LOG_TAG, "direction :" + direction);

                if (direction == 1) {
                    if (getTimeInterval(startPrevious) > 5 && startPrevious > 0) {
                        setVolume(audioManager,audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) + 1);
                        startPrevious = 0;
                    } else {
                        startPrevious = System.currentTimeMillis();
                    }
                } else if (direction == -1) {
                    if (getTimeInterval(startNext) > 5 && startNext > 0) {
                        setVolume(audioManager,audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) - 1);
                         startNext = 0;
                    } else {
                        startNext = System.currentTimeMillis();
                    }
                } else {
                    if (startPrevious > 0) {
                        startPrevious = resolvePressedVolumeButtons(startPrevious, true, 15);
                    }
                    if (startNext > 0) {
                        startNext = resolvePressedVolumeButtons(startNext, false, 0);
                    }
                }
            }
        };
    }
    private static double getTimeInterval(long t){
        return ((System.currentTimeMillis() - t) / 100.0);
    }
    private void setVolume(final AudioManager audioManager, int vol) {
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, vol, AudioManager.FLAG_PLAY_SOUND);
    }

    private long resolvePressedVolumeButtons(long startVolumeClick, final boolean isAddition, final int volumeFlag) {
        if (startVolumeClick > 0) {
            final double TIME_INTERVAL = getTimeInterval(startVolumeClick);
            Log.i(LOG_TAG, "TIME_INTERVAL :" + TIME_INTERVAL);
            int newVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            ;
            if (TIME_INTERVAL > 2.5) {
                if (isAddition)
                    playPrevious();
                else
                    playNext();
                final ToneGenerator tg = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 75);
                tg.startTone(ToneGenerator.TONE_PROP_BEEP2);
            } else {
                if (isAddition && newVolume < volumeFlag)
                    newVolume++;
                else if (!isAddition && newVolume > volumeFlag)
                    newVolume--;

            }
            Log.i(LOG_TAG, "new Vol :" + newVolume);
            setVolume(audioManager,newVolume);
        }
        return 0;
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        Toast.makeText(this, "Error playing :" + appStorage.getCurrentRadio().getStation(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }
}
