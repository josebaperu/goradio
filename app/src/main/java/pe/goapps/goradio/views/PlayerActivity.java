package pe.goapps.goradio.views;


import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import pe.goapps.goradio.R;
import pe.goapps.goradio.adapters.RadiosAdapter;
import pe.goapps.goradio.services.PlayerService;
import pe.goapps.goradio.store.AppStorage;
import pe.goapps.goradio.utils.Constants;

public class PlayerActivity extends AppCompatActivity {
    private static final String LOG_TAG = "PlayerActivity";
    private AppStorage appStorage = AppStorage.getInstance();
    private Intent service;
    private BroadcastReceiver updateUIReceiver;
    private TextView station_tv;
    private ImageButton status_img ;
    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(LOG_TAG,"OnCreate");
        setContentView(R.layout.activity_player);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        station_tv = findViewById(R.id.station);
        status_img =  findViewById(R.id.toggle);
        final RecyclerView recyclerView = findViewById(R.id.recycler_view);
        final RadiosAdapter adapter = new RadiosAdapter();

        final RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        status_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                service.setAction(Constants.ACTION.TOGGLE_PLAYBACK);
                startService(service);
            }
        });

        service = new Intent(PlayerActivity.this, PlayerService.class);
        service.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        service.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
        startService(service);

        IntentFilter filter = new IntentFilter();
        filter.addAction("updateplayer");
        filter.addAction("exit");
        updateUIReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                if(intent.getAction().equals("updateplayer"))
                    renderImageText();

                if(intent.getAction().equals("exit"))
                    stopActivity();
            }
        };
        registerReceiver(updateUIReceiver, filter);
        if(appStorage.isFirstRun()){
            showPermissionModal();
        }
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            final Intent powerIntent = new Intent();
            final String packageName = getPackageName();
            final PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                powerIntent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                powerIntent.setData(Uri.parse("package:" + packageName));
                startActivity(powerIntent);
            }
        }
        renderImageText();
    }

    private void promptAppPermissionsIntent() {
        final Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        final Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }


    private void renderImageText() {
        final boolean state = appStorage.isPlaybackOn();
        final String station = appStorage.getCurrentStation();
        station_tv.setText(station);
        status_img.setImageResource(state?R.drawable.pause:R.drawable.play);
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private final int spanCount;
        private final int spacing;
        private final boolean includeEdge;

        GridSpacingItemDecoration(final int spanCount, final int spacing, final boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(final Rect outRect, final View view, final RecyclerView parent, final RecyclerView.State state) {
            final int position = parent.getChildAdapterPosition(view); // item position
            final int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(final int dp) {
        final Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.i(LOG_TAG,"OnStart");

    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.i(LOG_TAG,"onResume");

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(LOG_TAG,"OnStop");

    }

    @Override
    protected void onRestoreInstanceState(final Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        Log.i(LOG_TAG,"OnRestore");

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(LOG_TAG,"OnDestroy");
            stopActivity();
    }
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Log.i(LOG_TAG,"OnBackPressed");
            stopActivity();
    }

    private void stopActivity() {
        unregisterReceiver(updateUIReceiver);
        stopService(service);
        finishAffinity();
        finishAndRemoveTask();
        System.exit(0);
    }

    private void showPermissionModal(){
        Log.i(LOG_TAG, "Dialog Opened");
        final String br ="\n";
        final String msg1 = getResources().getString(R.string.instructions_autoplay);
        final String msg2 = getResources().getString(R.string.instructions_toggle);
        final String msg3 = getResources().getString(R.string.instructions_permissions);
        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.instructions))
                .setCancelable(false)
                .setMessage(br+br+msg1+br+br+msg2+br+br+msg3+br)
                .setPositiveButton("OK", (dialog, which) -> {
                    Log.i(LOG_TAG, "Dialog Closed");
                    promptAppPermissionsIntent();

                })
                .show();
    }

}


