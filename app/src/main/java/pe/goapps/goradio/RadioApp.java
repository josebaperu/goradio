package pe.goapps.goradio;

import android.app.Application;
import android.content.Context;

import pe.goapps.goradio.store.AppStorage;

public class RadioApp extends Application {
    private static Application sApplication;

    public static Application getApplication() {
        return sApplication;
    }

    public static Context getContext() {
        return getApplication().getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Thread.setDefaultUncaughtExceptionHandler(
                (thread, e) -> AppStorage.getInstance().setIsPlaybackOn(false));
        sApplication = this;
    }
}
