package pe.goapps.goradio.model;

public class Radio {
    public static final String ID = "id";
    public static final String STATION = "station";
    public static final String COUNTRY = "country";
    public static final String IMG = "img";
    public static final String URL = "url";


    private int id;
    private String station;
    private String country;
    private String img;
    private String url;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
